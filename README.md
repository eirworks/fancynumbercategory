# FancyNumberCategory

This library is used to categorize fancy numbers (mobile phone numbers).

Supported categories (in Indonesian):

* Ilufa (168)
* Maju Mapan (5758)
* Seri Tahun (1900-2099)
* Seri Ulang Tahun (01-31/01-12/1900-2099)
* Triple
* Quartet
* Panca
* Hexa
* Septa
* Okta
* Nona
* Urut 4
* Urut 5
* Urut 6
* Urut 7
* Urut 8
* Urut 9
* Double AA (XXYY)
* Double AAA (XXXYYY)
* Double AAAA (XXXXYYYY)
* Double AAAAA (XXXXXYYYYY)
* Triple AA (XXYYZZ)
* Triple AAA (XXXYYYZZZ)
* Quartet AA (XXYYZZAA)
* Double AB (XYXY)
* Double ABC (XYZXYZ)
* Double ABCD (XYZAXYZA)
* Double ABCDE (XYZABXYZAB)
* Double ABCDEF (XYZABXYZABC)
* Triple AB (XYXYXY)
* Triple ABC (XYZXYZXYZ)
* Quartet AB (XYXYXYXY)
* Panca AB (XYXYXYXYXY)
* Eskalator (eg: 5xx6xx7xx or xx9x10x11)
* Tangga (eg: 5x6x7x or x91011)

## TODO

* Fix bug in checkTangga()
* Complete test

## Load

Load the class with:

`require '/path/to/src/FancyNumberCategory/FancyNumberCategory.php';`

Or use Composer.

`'eirworks/fancynumbercategory': 'dev-master'`

## Usage

FancyNumberCategory has many of function to determine whether a fancy number is in a category.

## License

MIT License

(c) eirworks