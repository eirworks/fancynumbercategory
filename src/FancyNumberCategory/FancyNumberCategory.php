<?php

// MIT license

namespace FancyNumberCategory; 

class FancyNumberCategory
{
	private static $categories = array(
			'ilufa',
			'majumapan',
			'tahun',
			'ulangtahun',
			'triple',
			'quartet',
			'panca',
			'hexa',
			'sapta',
			'okta',
			'nona',
			'urut4',
			'urut5',
			'urut6',
			'urut7',
			'urut8',
			'urut9',
			'doubleaa',
			'doubleaaa',
			'doubleaaaa',
			'doubleaaaaa',
			'tripleaa',
			'tripleaaa',
			'quartetaa',
			'doubleab',
			'doubleabc',
			'doubleabcd',
			'doubleabcde',
			'doubleabcdef',
			'tripleab',
			'tripleabc',
			'quartetab',
			'pancaab',
			'eskalator',
			'tangga',
		);
	
	/*
	* Check number whether it is on the category
	* @returns array
	*/
	public static function checkNumber($number)
	{
		
	}

	public static function getAvailableCategories()
	{
		return self::$categories;
	}

	public static function checkIlufa($number)
	{
		$number = self::prepNumber($number);
		if (preg_match('/1+6+8+/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkMajumapan($number)
	{
		$number = self::prepNumber($number);
		if (preg_match('/5+7+5+8+/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkTahun($number)
	{
		$number = self::prepNumber($number);
		if (self::_year($number) == 1)
		{
			return TRUE;
		}
		return FALSE;
	}

	public static function checkUlangTahun($number)
	{
		$number = self::prepNumber($number);
		if (self::_year($number) == 2)
		{
			return TRUE;
		}
		return FALSE;
	}

	private static function _year($number)
	{
		$tahun = 0;
        if ( preg_match("/(19|20)[0-9][0-9]$/",$number) )
        {
            $tahun = 1;
        }
        if ( preg_match('/(([0-2][0-9])|(3[0-1]))((0[1-9])|1[0-2])(19|20)[0-9]{2}$/',$number) )
        {
            $tahun = 2;
        }
        return $tahun;
	}

	public static function checkTriple($number)
	{
		return self::_repeating($number,3);
	}

	public static function checkQuartet($number)
	{
		return self::_repeating($number,4);
	}

	public static function checkPanca($number)
	{
		return self::_repeating($number,5);
	}

	public static function checkHexa($number)
	{
		return self::_repeating($number,6);
	}

	public static function checkSepta($number)
	{
		return self::_repeating($number,7);
	}

	// alias for checkSepta()
	public static function checkSapta($number)
	{
		return self::checkSepta($number);
	}

	public static function checkOkta($number)
	{
		return self::_repeating($number,8);
	}

	public static function checkNona($number)
	{
		return self::_repeating($number,9);
	}

	private static function _repeating($number,$repeat=3,$maxRepeat = 10)
	{
		$number = self::prepNumber($number);

		$repeatVal = 0;
		for($i = 3; $i<= $maxRepeat; $i++)
		{
			if ( preg_match('/(\d)\1{'.($i-1).'}$/',$number) )
			{
				$repeatVal = $i;
			}
		}

		if ($repeatVal == $repeat)
			return TRUE;
		return FALSE;
	}

	public static function checkUrut4($number)
	{
		return self::_sequence($number,4);
	}

	public static function checkUrut5($number)
	{
		return self::_sequence($number,5);
	}

	public static function checkUrut6($number)
	{
		return self::_sequence($number,6);
	}

	public static function checkUrut7($number)
	{
		return self::_sequence($number,7);
	}

	public static function checkUrut8($number)
	{
		return self::_sequence($number,8);
	}

	public static function checkUrut9($number)
	{
		return self::_sequence($number,9);
	}

	private static function _sequence($number,$sequence=4)
	{
		$number = self::prepNumber($number);
		$reversedNumber = strrev($number);

		$sequences = array(
            4 => array(
                '0123','1234','2345','3456','4567','5678','6789',
                '9876','8765','7654','6543','5432','4321','3210'
            ),
            5 => array(
                '01234','12345','23456','34567','45678','56789',
                '98765','87654','76543','65432','54321','43210'
            ),
            6 => array(
                '012345','123456','234567','345678','456789',
                '987654','876543','765432','654321','543210'
            ),
            7 => array(
                '0123456','1234567','2345678','3456789',
                '9876543','8765432','7654321','6543210'
            ),
            8 => array(
                '01234567','12345678','23456789',
                '98765432','87654321','76543210',
            ),
            9 => array(
                '012345678','123456789',
                '987654321','876543210'
            )
        );

		$sequenceVal = 0;
		for($i=4;$i<=9;$i++)
		{
			$testedN = substr($number, (-1 * $i));
			if (in_array($testedN,$sequences[$i]))
			{
				$sequenceVal = $i;
			}
		}
		if ($sequenceVal == $sequence)
		{
			return TRUE;
		}
		return FALSE;
	}

	public static function checkDoubleaa($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)\1((?!\1)\d)\2$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleaaa($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)\1\1((?!\1)\d)\2\2$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleaaaa($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)\1\1\1((?!\1)\d)\2\2\2$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleaaaaa($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)\1\1\1\1((?!\1)\d)\2\2\2\2$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkTripleaa($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)\1((?!\1)\d)\2((?!\2)\d)\3$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkTripleaaa($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)\1\1((?!\1)\d)\2\2((?!\2)\d)\3\3$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkQuartetaa($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)\1((?!\1)\d)\2((?!\2)\d)\3((?!\3)\d)\4$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleab($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)((?!\1)\d)\1\2$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleabc($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match('/(\d)((?!\1)\d)((?!\2)\d)\1\2\3$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleabcd($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match( '/(\d)((?!\1)\d)((?!\2)\d)((?!\3)\d)\1\2\3\4$/',$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleabcde($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match( '/(\d)((?!\1)\d)((?!\2)\d)((?!\3)\d)((?!\4)\d)\1\2\3\4\5$/' ,$number))
			return TRUE;
		return FALSE;
	}

	public static function checkDoubleabcdef($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match( '/(\d)((?!\1)\d)((?!\2)\d)((?!\3)\d)((?!\4)\d)((?!\5)\d)\1\2\3\4\5\6$/' ,$number))
			return TRUE;
		return FALSE;
	}

	public static function checkTripleab($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match( '/(\d)((?!\1)\d)\1\2\1\2$/' ,$number))
			return TRUE;
		return FALSE;
	}

	public static function checkTripleabc($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match( '/(\d)((?!\1)\d)((?!\2)\d)\1\2\3\1\2\3$/' ,$number))
			return TRUE;
		return FALSE;
	}

	public static function checkQuartetab($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match( '/(\d)((?!\1)\d)\1\2\1\2\1\2$/' ,$number))
			return TRUE;
		return FALSE;
	}

	public static function checkPancaab($number)
	{
		$number = self::prepNumber($number);
		if ( preg_match( '/(\d)((?!\1)\d)\1\2\1\2\1\2\1\2$/' ,$number))
			return TRUE;
		return FALSE;
	}

	public static function checkTangga($number)
	{
		$number = self::prepNumber($number);

		$val = FALSE;
        $tangga = str_split(substr($number,-6));
        if (($tangga[0] == $tangga[2]) && ($tangga[2] == $tangga[4]))
        {
            if ( (($tangga[1]+1) == $tangga[3] && ($tangga[3]+1) == $tangga[5]) || ($tangga[1]-1) == $tangga[3] && ($tangga[3]-1) == $tangga[5] )
            {
                $val = TRUE;
            }
        }
        if (($tangga[1] == $tangga[3]) && ($tangga[3] == $tangga[5]))
        {
            if ( (($tangga[0]+1) == $tangga[2] && ($tangga[2]+1) == $tangga[4]) || ($tangga[0]-1) == $tangga[2] && ($tangga[2]-1) == $tangga[4] )
            {
                $val = TRUE;
            }
        }

        if ($val)
        	return TRUE;
        return FALSE;
	}

	public static function checkEskalator($number)
	{
		$number = self::prepNumber($number);

        $eskalator = str_split(substr($number,-9));
        // same first char
        if (($eskalator[0] == $eskalator[3]) && ($eskalator[3] == $eskalator[6]))
        {
            //group last 2
            if ( (((int)$eskalator[1].$eskalator[2]) - 1) == ((int)$eskalator[4].$eskalator[5]) && (((int)$eskalator[4].$eskalator[5]) - 1) == ((int)$eskalator[7].$eskalator[8]))
            {
                return TRUE;
            }
            if ( (((int)$eskalator[1].$eskalator[2]) + 1) == ((int)$eskalator[4].$eskalator[5]) && (((int)$eskalator[4].$eskalator[5]) + 1) == ((int)$eskalator[7].$eskalator[8]))
            {
                return TRUE;
            }
            if ( ($eskalator[1] == $eskalator[4]) && ($eskalator[4] == $eskalator[7]))
            {
                if ( (($eskalator[2]+1) == $eskalator[5]) && ( ($eskalator[5]+1) == $eskalator[8]))
                {
                    return TRUE;
                }
                if ( (($eskalator[2]-1) == $eskalator[5]) && ( ($eskalator[5]-1) == $eskalator[8]))
                {
                   return TRUE;
                }
            }
            if ( ($eskalator[2] == $eskalator[5]) && ($eskalator[5] == $eskalator[8]))
            {
                if ( (($eskalator[1]+1) == $eskalator[4]) && ( ($eskalator[4]+1) == $eskalator[7]))
                {
                    return TRUE;
                }
                if ( (($eskalator[1]-1) == $eskalator[4]) && ( ($eskalator[4]-1) == $eskalator[7]))
                {
                   return TRUE;
                }
            }
        }
        if ( (((int)$eskalator[1].$eskalator[2]) == ((int)$eskalator[4].$eskalator[5])) && (((int)$eskalator[4].$eskalator[5]) == ((int)$eskalator[7].$eskalator[8])) )
        {
            if (((($eskalator[0]+1) == $eskalator[3]) && (($eskalator[3]+1) == $eskalator[6])) || (($eskalator[0]-1) == $eskalator[3]) && (($eskalator[3]-1) == $eskalator[6]))
            {
                return TRUE;
            }
        }
        return FALSE;
	}

	/*
	* Prepare number to be indexable, processable
	*/
	public static function prepNumber($rawNumber)
	{
		// $rawNumber = strtoupper($rawNumber);
		$rawNumber = str_replace('A', '4', $rawNumber);
		$rawNumber = str_replace('B', '8', $rawNumber);
		$rawNumber = str_replace('E', '3', $rawNumber);
		$rawNumber = str_replace('G', '6', $rawNumber);
		$rawNumber = str_replace('g', '9', $rawNumber);
		$rawNumber = str_replace('i', '1', $rawNumber);
		$rawNumber = str_replace('j', '7', $rawNumber);
		$rawNumber = str_replace('l', '1', $rawNumber);
		$rawNumber = str_replace('L', '1', $rawNumber);
		$rawNumber = str_replace('O', '0', $rawNumber);
		$rawNumber = str_replace('S', '5', $rawNumber);
		return preg_replace('/[^0-9]/', "", self::filterNumber($rawNumber) );
	}

	/*
	* Filter things other than space, number, and alphabets
	*/
	public static function filterNumber($rawNumber)
	{
		$number = preg_replace('/[^0-9a-zA-Z\ ]/', "", trim($rawNumber) );
		return preg_replace('/\s+/',' ',$number);
	}
}