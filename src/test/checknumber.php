<?php 

require __DIR__."/../FancyNumberCategory/FancyNumberCategory.php";

// use \FancyNumberCategory;

echo \FancyNumberCategory\FancyNumberCategory::filterNumber("081 O123 * XXX 123");
echo "\n";
echo \FancyNumberCategory\FancyNumberCategory::prepNumber("081 O123 * XXX 123");
echo "\n";

$numbers = array(
	// nothing
	'081 555 150 150',
	// ilufa
	'081 555 11 66 88',
	// majumapan
	'0812 1234 5758',
	// tahun
	'0812 12 44 1989',
	// ulangtahun
	'0812 12 04 1989',
	// repeating
	'0812 12 04 0333',
	'0812 12 04 3333',
	'0812 12 03 3333',
	'0812 12 33 3333',
	'0812 13 33 3333',
	'0812 33 33 3333',
	'0813 33 33 3333',

	// sequences
	'081 1111 1234',
	'081 009 12345',
	'081 09 123456',
	'081 9 1234567',
	'081 12345678',
	'081 23456789',

	//AA
	'0811 1234 0088',
	'0811 12 000888',
	'0811 00008888',
	'0811 000222333',
	'08 11111 33333',

	// eskalator
	'081 101 102 103',
	'081 110 109 108',
	'081 212 312 412',

	//tangga
	'0811 89 88 87',
	'0811 90 89 89',
	'0811 45 46 47',
);

print_r(\FancyNumberCategory\FancyNumberCategory::getAvailableCategories());

$tests = array(
	'ilufa' => [ '081 555 11 66 88', '081 555 11 6168' ],
	'majumapan' => [ '081 555 11 5758', '08123 55775588'],
	'tahun' => [  ],
	'ulangtahun'=> [ ],
	'triple'=> [ ],
	'quartet'=> [ ],
	'panca'=> [ ],
	'hexa'=> [ ],
	'sapta'=> [ ],
	'okta'=> [ ],
	'nona'=> [ ],
	'urut4'=> [ ],
	'urut5'=> [ ],
	'urut6'=> [ ],
	'urut7'=> [ ],
	'urut8'=> [ ],
	'urut9'=> [ ],
	'doubleaa'=> [ ],
	'doubleaaa'=> [ ],
	'doubleaaaa'=> [ ],
	'doubleaaaaa'=> [ ],
	'tripleaa'=> [ ],
	'tripleaaa'=> [ ],
	'quartetaa'=> [ ],
	'doubleab'=> [ ],
	'doubleabc'=> [ ],
	'doubleabcd'=> [ ],
	'doubleabcde'=> [ ],
	'doubleabcdef'=> [ ],
	'tripleab'=> [ ],
	'tripleabc'=> [ ],
	'quartetab'=> [ ],
	'pancaab'=> [ ],
	'eskalator'=> [ ],
	'tangga'=> [ ],
);

foreach($tests as $testName => $numbers)
{
	if (count($numbers) > 0)
	{
		echo "Testing $testName\n";
		foreach($numbers as $num)
		{
			$test = "check".(ucfirst($testName));
			if (\FancyNumberCategory\FancyNumberCategory::$test($num))
			{
				echo "$num is $testName\n";
			}
			else
			{
				echo "$num is not $testName, fail\n";
			}
		}
		echo "\n";
	}
}